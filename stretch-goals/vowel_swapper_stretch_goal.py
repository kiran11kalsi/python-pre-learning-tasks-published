def vowel_swapper(string):
    # ==============
    string = string.replace("aAa", "a/\\a").replace("vai", "v/\\!").replace("eEe", "e3e").replace("ve", "v3").replace("I", "!").replace("O", "000").replace("or", "ooor").replace("U", "\/")
    return string

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
